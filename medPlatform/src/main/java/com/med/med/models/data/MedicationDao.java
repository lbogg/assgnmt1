package com.med.med.models.data;

import com.med.med.models.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Repository
@Transactional
public interface MedicationDao extends JpaRepository<Medication,Long> {
    @Override
    Optional<Medication> findById(Long aLong);

    List<Medication> findAllByPatientId(Long aLong);
}
