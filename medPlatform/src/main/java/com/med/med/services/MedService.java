package com.med.med.services;

import com.med.med.utils.RegisterDto;
import com.med.med.exceptions.GenericException;
import com.med.med.models.Medication;
import com.med.med.models.Role;
import com.med.med.models.User;
import com.med.med.models.data.MedicationDao;
import com.med.med.models.data.UserDao;
import com.med.med.utils.MedicationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@Service
public class MedService {
    private UserDao userDao;
    private MedicationDao medicationDao;

    @Autowired
    public MedService(UserDao userDao,MedicationDao medicationDao){
        this.userDao = userDao;
        this.medicationDao=medicationDao;
    }

    public List<User> findPatients() {return userDao.findAllByRole(Role.PATIENT);}

    public List<User> findCaregivers() {return userDao.findAllByRole(Role.CAREGIVER);}

    public List<Medication> findMedByPatientId() {return medicationDao.findAllByPatientId((long)1);}

    public void createUser(RegisterDto registerDto) throws GenericException {
        User user = new User();
        if(registerDto.getName() == null || registerDto.getName().length() < 3 ||
                registerDto.getUsername() == null || registerDto.getUsername().length() < 3 ||
                registerDto.getPassword() == null || registerDto.getPassword().length() < 3) {
            throw new GenericException("All fields must have al least 3 characters.");
        }

        user.setRole(Role.PATIENT);
        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setName(registerDto.getName());

        userDao.save(user);
    }

    public void removeUser(long id){userDao.deleteById(id);}

    public Medication makeMedication(MedicationDto medicationDto){
        Medication medication = new Medication();
        medication.setPatient(userDao.findById(medicationDto.getUserId()).orElseThrow(EntityNotFoundException::new));
        medication.setName(medicationDto.getName());
        medication.setDosage(medicationDto.getDosage());
        medication.setPeriodStart(medicationDto.getPeriodStart());
        medication.setPeriodEnd(medicationDto.getPeriodEnd());
        medicationDao.save(medication);
        return medication;
    }

    public void deleteMed(long id){
        Medication medication = this.medicationDao.findById(id).orElseThrow(EntityNotFoundException::new);

        for(Medication m:medication.getPatient().getMedicalRecord()){
            User patient = medication.getPatient();
            patient.getMedicalRecord().clear();

            this.userDao.save(patient);
        }
        this.medicationDao.delete(medication);
    }

    public void deleteUser(long id){
        User user = this.userDao.findById(id).orElseThrow(EntityNotFoundException::new);

        if(user.getRole() == Role.PATIENT || user.getRole() == Role.CAREGIVER){
            userDao.delete(user);
        }
    }
}
