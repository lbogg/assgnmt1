package com.med.med.controller;

import com.med.med.services.MedService;
import com.med.med.models.Role;
import com.med.med.models.User;
import com.med.med.models.data.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;
import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {
    @Autowired
    private UserDao userDao;
    @Autowired
    private MedService medService;

    @GetMapping
    public List<User> displayUsers(){
        return userDao.findAll();
    }

    @GetMapping("/patients")
    public List<User> displayPatients(){return userDao.findAllByRole(Role.PATIENT);}

    @GetMapping("/profile")
    public User profile() {
        return userDao.findByUsername(((UserDetails)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername()).orElseThrow(EntityNotFoundException::new);
    }

    @PostMapping
    public void create(@RequestBody User user) {
        User u = new User();
        u.setRole(user.getRole());
        u.setUsername(user.getUsername());
        u.setPassword(user.getPassword());
        u.setName(user.getName());
        userDao.save(u);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable long id){
        medService.deleteUser(id);
    }


}
