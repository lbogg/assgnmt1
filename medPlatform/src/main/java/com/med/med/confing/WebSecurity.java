package com.med.med.confing;

import com.med.med.models.Role;
import com.med.med.models.User;
import com.med.med.models.data.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;
import java.util.Collections;

@Configuration
@EnableWebSecurity
//@EnableJpaRepositories(basePackages = {"models.data"})
public class WebSecurity extends WebSecurityConfigurerAdapter {

    private final UserDao userDao;

    @Autowired
    public WebSecurity(UserDao userDao) {
        this.userDao = userDao;
    }

    public static class UserDetailsImpl implements UserDetails {
        final User user;

        public UserDetailsImpl(User user) {
            this.user = user;
        }

        @Override
        public Collection<? extends GrantedAuthority> getAuthorities() {
            return Collections.singletonList(new SimpleGrantedAuthority("ROLE_" + user.getRole().name()));
        }

        @Override
        public String getPassword() {
            return user.getPassword();
        }

        @Override
        public String getUsername() {
            return user.getUsername();
        }

        @Override
        public boolean isAccountNonExpired() {
            return true;
        }

        @Override
        public boolean isAccountNonLocked() {
            return true;
        }

        @Override
        public boolean isCredentialsNonExpired() {
            return true;
        }

        @Override
        public boolean isEnabled() {
            return true;
        }
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception{
        httpSecurity.authorizeRequests()
//                .antMatchers("/api/login*", "/api/login/**", "/api/register").anonymous()
                .antMatchers("/api/users").hasRole(Role.DOCTOR.name())
               .antMatchers("/api/users/*").hasRole(Role.DOCTOR.name())
                //.antMatchers("/api/users/profile").hasRole(Role.PATIENT.name())
                .antMatchers("/api/medication").hasRole(Role.DOCTOR.name())
                .antMatchers("/api/medication/*").hasRole(Role.DOCTOR.name())
                .antMatchers(HttpMethod.DELETE, "/api/users/*").hasRole(Role.DOCTOR.name())
                .antMatchers(HttpMethod.GET, "/api/users/*").hasAnyRole(Role.DOCTOR.name(), Role.CAREGIVER.name())
                .anyRequest().authenticated()
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .httpBasic()
                    .authenticationEntryPoint((request, response, authException) -> {
                        response.sendError(HttpStatus.UNAUTHORIZED.value(), HttpStatus.UNAUTHORIZED.getReasonPhrase());
                    })
                .and().csrf().disable();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder authentication) throws Exception {
        authentication.userDetailsService(new UserServiceImpl());
        // authentication.userDetailsService(username ->new UserDetailsImpl(userDao.findByUsername(username).orElseThrow(EntityNotFoundException::new)));
    }

    class UserServiceImpl implements UserDetailsService {

        @Override
        public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
            User user = userDao.findByUsername(username).orElseThrow(EntityNotFoundException::new);

            return new UserDetailsImpl(user);
        }
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new PasswordEncoder() {
            @Override
            public String encode(CharSequence rawPassword) {
                return rawPassword.toString();
            }

            @Override
            public boolean matches(CharSequence rawPassword, String encodedPassword) {
                return rawPassword.toString().equals(encodedPassword);
            }
        };
    }
}