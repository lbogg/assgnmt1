package com.example.server;

import com.med.med.models.Medication;
import com.med.med.models.User;
import com.med.med.models.data.MedicationDao;
import com.med.med.models.data.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@Service
public class MedRepo {

    public List<Medication> getMedications(long patientId){
        List<Medication> medications = new ArrayList<>();
        String querry = "Select * from medication where patient_id = ?";
       // String querry1 = "Select * from user where id = ?";
        try {
            Connection connection = ConnectionManager.getConnection();
            System.out.println(connection);
            PreparedStatement preparedStatement = connection.prepareStatement(querry);
            preparedStatement.setLong(1,patientId);
            //PreparedStatement preparedStatement1 = connection.prepareStatement(querry1);
            //preparedStatement1.setLong(1,patientId);
            ResultSet resultSet = preparedStatement.executeQuery();
            //ResultSet resultSet1 = preparedStatement1.executeQuery();
            while(resultSet.next()){
                long medId = resultSet.getLong("id");
                long pId = resultSet.getLong("patient_id");
                String startTime = resultSet.getString("period_start");
                String endTime = resultSet.getString("period_end");
                String name = resultSet.getString("name");
                Medication medication = new Medication();
                User user = new User();
                user.setId(pId);
               // user.setName(resultSet1.getString("name"));
                //user.setUsername(resultSet1.getString("username"));
                medication.setId(medId);
                medication.setName(name);
                medication.setPatient(user);
                medication.setPeriodStart(startTime);
                medication.setPeriodEnd(endTime);
                medications.add(medication);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return medications;
    }
}
