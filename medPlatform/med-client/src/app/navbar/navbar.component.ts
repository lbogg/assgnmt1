import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import {User} from "../models/user.model";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-navbar',
  template: `
  <nav class="navbar navbar-inverse">
    <div class="container-fluid ">
      <div class="navbar-header">
        <a class="navbar-brand" href="#">Medical Platform</a>
      </div>
      <ul class="nav navbar-nav">
        <li class="nav-item" *ngIf="logged && user.role === 'DOCTOR'" routerLinkActive="active">
          <a class="nav-link" [routerLink]="['/patients']">Users</a>
        </li>
        <li class="nav-item" *ngIf="logged && user.role === 'CAREGIVER'" routerLinkActive="active">
          <a class="nav-link" [routerLink]="['/users/patients']">Patients</a>
        </li>
        <li class="nav-item" *ngIf="logged && user.role === 'PATIENT'" routerLinkActive="active">
          <a class="nav-link" [routerLink]="['/medication/my-medication']">Adauga Companie</a>
        </li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
       <!-- <li *ngIf="!logged" class="nav-item"><a class="nav-link" [routerLink]="['/register-user']" href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>-->
        <li *ngIf="!logged" class="nav-item"><a class="nav-link" [routerLink]="['/login']" href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
        <li *ngIf="logged" class="nav-item"><a class="nav-link" [routerLink]="['/login']" (click)="logout()" href="#"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
        <li *ngIf="logged" class="nav-item">
          <strong class="nav-link">{{user.name}}</strong>
        </li>
      </ul>
    </div>
  </nav>
  `,
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  logged = false;
  user: User;

  constructor(public auth: AuthService, private http: HttpClient) { }

  ngOnInit() {
    this.auth.loggedStatus.subscribe(isLogged => {
      this.logged = !!isLogged;
      this.user = isLogged;
    });
  }

  logout() {
    this.auth.loggedStatus.next(null);
    localStorage.clear();
  }

}
