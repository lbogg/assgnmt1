import {User} from "./user.model";

export interface MedicationDto {
  userId: number;
  name: string;
  dosage: number;
  period: string;
}

export interface Medication {
  id: number;
  name: string;
  sideEffects: string;
  dosage: number;
  period: string;
  patient: User;
}
